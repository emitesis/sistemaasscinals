import React, {Component} from 'react'
import {
  CAlert,
  CBadge,
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CCardFooter,
  CCol,
  CDataTable,
  CForm,
  CFormGroup,
  CInput,
  CLabel,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CSelect,
  CRow,
} from '@coreui/react'
import axios from 'axios'
import md5 from 'md5'
import CIcon from '@coreui/icons-react'
import Cookies from 'universal-cookie';
const cookies = new Cookies();

const fields = ['nombreUsuario','rol', 'estado', 'accion']
const url="http://192.168.0.79:8000/usuario/listar/";
const url1="http://192.168.0.79:8000/usuario/crear/";
const url2="http://192.168.0.79:8000/usuario/modificar/";

class RegsitroUsuarioInterno extends Component {

  state={
    modal1: false,
    modal2: false,
    modal3: false,
    alert1: false,
    alert2: false,
    alert3: false,
    msj:'',
    data:[],
    form:{
      id:'',
      nombreUsuario:'',
      contraseña:'',
      email:'',
      privilegio:'',
      verificacion: 'sin.verificar',
      estado: 'true',
      idUsuario: 'x'
    }
  };

  modal1Visible= ()=>{
    this.setState({modal1: !this.state.modal1});
  }

  modal2Visible= ()=>{
    this.setState({modal2: !this.state.modal2});
  }

  modal3Visible= ()=>{
    this.setState({modal3: !this.state.modal3});
  }

  alert1Visible= ()=>{
    this.setState({alert1: !this.state.alert1});
  }

  alert2Visible= ()=>{
    this.setState({alert2: !this.state.alert2});
  }

  alert3Visible= (e)=>{
    this.setState({alert3: !this.state.alert3});
    this.setState({msj: e})
  }

  getPriv = (status) => {
    switch (status) {
      case 0: return 'Huesped'
      case 1: return 'Presidente'
      default: return 'Secretaria'
    }
  }
  getEstado = (status) => {
    switch (status) {
      case true: return 'Habilitado'
      default: return 'Bloqueado'
    }
  }
  getColorEstado = (status) => {
    switch (status) {
      case true: return 'success'
      default: return 'danger'
    }
  }
  handleChange = async (e) =>{
    await this.setState({
      form:{
        ...this.state.form,
        [e.target.name]: e.target.value
      }
    });
  }

  setUsuario = async (usuario) => {
    await this.setState({
      form: {
        id: usuario.id,
        nombreUsuario: usuario.nombreUsuario,
        contraseña: usuario.contraseña,
        email: usuario.email,
        privilegio: usuario.privilegio,
        verificacion: usuario.verificacion,
        estado: !usuario.estado,
        idUsuario: usuario.idUsuario
      }
    })
    this.modal2Visible()
  }

  contraseñaHandleChange = async (e) =>{
    await this.setState({
      form:{
        ...this.state.form,
        contraseña: md5(e.target.value)
      }
    });
  }

  usuarioHandleChange = async (e) =>{
    await this.setState({
      form:{
        ...this.state.form,
        nombreUsuario: e.target.value
      }
    });
    this.emailHandleChange(e);
  }

  emailHandleChange = async(e) =>{
    await this.setState({
      form:{
        ...this.state.form,
        email: e.target.value
      }
    });
  }

  modificarUsuario=()=>{
        axios.post(url2+this.state.form.id+"/", this.state.form)
        .then(response=>{
            console.log(response.data)
            this.modal2Visible();
            this.addDatos();
        })
        .catch(error=>{
            console.log(error.response.data);
        })
  }

  registrarUsuario=async()=>{
    let noValido = /\s/;
    if(noValido.test(this.state.form.nombreUsuario)){
      this.alert3Visible('El nombre de usuario no debe contener espacios.')
    }else{
      if(this.state.form.privilegio.length>0){
        if(this.state.form.contraseña.length>5){
          delete this.state.form.id;
          await axios.post(url1, this.state.form)
          .then(response=>{
              console.log(response.data)
              this.addDatos();
              this.modal1Visible();
              this.modal3Visible();
          })
          .catch(error=>{
              this.alert1Visible()
              console.log(error.response.data);
          })
        }else{
          this.alert2Visible()
        }
      }else{
        this.alert3Visible('No selecciono el rol del usuario.')
      }
    }    
  }

  addDatos=()=>{    
    axios.get(url)
    .then(response=>{
      this.setState({data: response.data})
    }).catch(error=>{
      console.log(error);
    })
  }

  componentDidMount(){
    this.addDatos();
    if(cookies.get('privilegio').toString() !== '1'){      
      window.location.assign("./error404");
    }
  }
  render(){
    return (
        <CRow >
          <CCol >
          <CCard>
            <CCardHeader>
              Lista de Usuario
            </CCardHeader>
            <CCardBody>
              
              <CModal 
              show={this.state.modal1} 
              onClose={this.modal1Visible}
              color="primary"
              >
                  <CModalHeader closeButton>
                      <CModalTitle>Registro de usuario</CModalTitle>
                  </CModalHeader>
                  <CModalBody>
                      <CCard>
                          <CCardBody>
                              <CForm action="" encType="multipart/form-data" className="form-horizontal">
                                  <CFormGroup>
                                  <h6> Crea un nuevo usuario</h6>
                                  </CFormGroup>
                                  <CFormGroup row>
                                  <CCol md="3">
                                      <CLabel htmlFor="text-input">Rol</CLabel>
                                  </CCol>
                                  <CCol xs="12" md="9">
                                      <CSelect custom name="privilegio" id="privilegio" onChange={this.handleChange}>
                                      <option value="Ninguno">Seleccionar rol</option>
                                      <option value="1">Presidente</option>
                                      <option value="2">Secretaria</option>
                                      </CSelect>
                                  </CCol>
                                  </CFormGroup>
                                  <CFormGroup row>
                                  <CCol md="3">
                                      <CLabel htmlFor="text-input">Nombre de Usuario</CLabel>
                                  </CCol>
                                  <CCol xs="12" md="9">
                                      <CInput id="NombreUsuario" name="nombreUsuario" placeholder="Ingresar usuario" onChange={this.usuarioHandleChange}/>
                                  </CCol>
                                  </CFormGroup>                                  
                                  <CFormGroup row>
                                  <CCol md="3">
                                      <CLabel htmlFor="text-input">Contraseña</CLabel>
                                  </CCol>
                                  <CCol xs="12" md="9">
                                      <CInput type="password" name="contraseña" placeholder="Ingresar contraseña" onChange={this.contraseñaHandleChange}/>
                                  </CCol>
                                  </CFormGroup>
                                  <CAlert
                                    color="info"
                                    show={this.state.alert1}
                                  >   El usuario ya existe.
                                  </CAlert>
                                  <CAlert
                                    color="info"
                                    show={this.state.alert2}
                                  >   Las contraseñas debe tener minimo 6 caracteres.
                                  </CAlert>
                                  <CAlert
                                    color="info"
                                    show={this.state.alert3}
                                  >  {this.state.msj}
                                  </CAlert>
                              </CForm>
                          </CCardBody>
                      </CCard>
                  </CModalBody>
                  <CModalFooter>
                      <CButton variant="outline" color="info" onClick={this.registrarUsuario}>Guardar</CButton>{' '}
                      <CButton variant="outline" color="danger" onClick={this.modal1Visible}>Cancelar</CButton>
                  </CModalFooter>
                </CModal>
              
              <CModal 
              show={this.state.modal2} 
              onClose={this.modal2Visible}
              color="warning"
              size="sm"
              >
                <CModalHeader closeButton>
                  <CModalTitle>Bloqueo Usuario</CModalTitle>
                </CModalHeader>
                <CModalBody>
                  ¿Estas seguro de bloquear al usuario?
                </CModalBody>
                <CModalFooter>
                  <CButton variant="outline" color="info" onClick={()=>this.modificarUsuario()}>Bloquear</CButton>{' '}
                  <CButton variant="outline" color="danger" onClick={this.modal2Visible}>Cancelar</CButton>
                </CModalFooter>
              </CModal>

              <CModal 
              show={this.state.modal3} 
              onClose={this.modal3Visible}
              color="success"
              size="sm"
              >
                <CModalHeader closeButton>
                  <CModalTitle>Registro Usuario</CModalTitle>
                </CModalHeader>
                <CModalBody>
                  Registro de usuario exitoso
                </CModalBody>
              </CModal>

              <CButton color="success" className="mr-1" onClick={this.modal1Visible}>
                <CIcon name="cil-pencil"/>{' '}Registrar Usuario
              </CButton>
              <hr />
              <CDataTable
                    items={this.state.data}
                    fields={fields}
                    itemsPerPage={5}
                    pagination
                    footer
                    tableFilter
                    scopedSlots = {{
                      'nombreUsuario':
                      (item)=>(
                          <td>
                              @{item.nombreUsuario}
                          </td>
                      ),
                      'rol':
                        (item)=>(
                            <td>
                                {this.getPriv(item.privilegio)}
                            </td>
                      ),
                      'estado':
                        (item)=>(
                            <td>
                              <CBadge color={this.getColorEstado(item.estado)}>
                                {this.getEstado(item.estado)}
                              </CBadge>
                            </td>
                        ),
                      'accion':
                        (item)=>(
                            <td>
                              <CButton variant="outline" color="dark" hidden={!item.estado} size="sm" onClick={()=>this.setUsuario(item)}><CIcon name="cil-user-x"/>Bloquear</CButton>
                            </td>
                        )

                    }}
                />
            </CCardBody>
            <CCardFooter>
              
            </CCardFooter>
          </CCard>
        </CCol>
        </CRow>
    )
  }
}

export default RegsitroUsuarioInterno