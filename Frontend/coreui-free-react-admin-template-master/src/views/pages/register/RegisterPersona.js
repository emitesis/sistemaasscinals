import React, {Component} from 'react'
import {
  CBreadcrumb,
  CBreadcrumbItem,
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CFormGroup,
  CInput,
  CInputFile,
  CLabel,
  CSelect,
  CRow
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import axios from 'axios'
import Cookies from 'universal-cookie'
const cookies = new Cookies();
const fd = new FormData();
const url="http://192.168.0.79:8000/usuario/huesped/crear/";
const url1="http://192.168.0.79:8000/usuario/listar/";
const url2="http://192.168.0.79:8000/usuario/modificar/";

class Register extends Component {

  /*const[nombre, setNombre]=useState(null);
  const[apellido, setApellido]=useState(null);
  const[grado, setGrado]=useState(null);
  const[genero, setGenero]=useState(null);
  const[carnetIdentidad, setCarnetIdentidad]=useState(null);
  const[carnetMilitar, setCarnetMilitar]=useState(null);
  const[documento, setDocumento]=useState(null);

  /*const addDatos=async()=>{
    
    fd.append('nombre',nombre);
    fd.append('apellido',apellido);
    fd.append('genero',genero);
    fd.append('grado',grado);    
    fd.append('carnetIdentidad',carnetIdentidad);
    fd.append('carnetMilitar',carnetMilitar);
    fd.append('documento',documento);
    
    await axios.post(url, fd)
    .then(response=>{
      console.log(response.data);
      cookies.remove('verificacion', {path: "/"});
      cookies.remove('idPersona', {path: "/"});
      cookies.set('verificacion', verif, {path: "/"});
      cookies.set('idPersona', response.data.id, {path: "/"});
      cookies.set('estRegis', 'si', {path: "/"});
      window.location.href = "./"      
    }).catch(error=>{
      console.log(error.response.data);
    })
  }*/
  state={  
    dataHuesped:[],
    dataUsuario:[],
    formUsuario:{ 
      nombreUsuario:'',
      contraseña:'',
      email:'',
      privilegio:'',
      verificacion:'',
      estado:'',
      idUsuario:''
    },
    form:{ 
      nombre:'',
      apellido:'',
      genero:'',
      grado:'',
      carnetIdentidad:'',
      carnetMilitar:'',
      documento:''
    }
  };
  handleChange = async (e) =>{
    await this.setState({
        form:{
            ...this.state.form,
            [e.target.name]: e.target.value
        }
    });
  }
  handleChangeFile = async (e) =>{
    await this.setState({
        form:{
            ...this.state.form,            
            [e.target.name]: e.target.files[0]
        }
    });
  }
  llenarForm=async(e)=>{
    await fd.append('nombre',this.state.form.nombre);
    await fd.append('apellido',this.state.form.apellido);
    await fd.append('genero',this.state.form.genero);    
    await fd.append('grado',this.state.form.grado);
    await fd.append('carnetIdentidad',this.state.form.carnetIdentidad);
    await fd.append('carnetMilitar',this.state.form.carnetMilitar);
    await fd.append('documento',this.state.form.documento); 
  }
  modificarUsuario=async(dHuesped,dUser)=>{
    let result = dUser.filter((obj) => obj.id === parseInt(cookies.get('id')));
    let dataU = result[0]
    await this.setState({
        formUsuario:{
            ...this.state.formUsuario,
            nombreUsuario:dataU.nombreUsuario,
            contraseña:dataU.contraseña,
            email:dataU.email,
            privilegio:dataU.privilegio,
            verificacion:'en.progreso',
            estado: true,
            idUsuario:dHuesped.id
        }
    });
    await axios.post(url2+dataU.id+'/', this.state.formUsuario)
    .then(response=>{
        console.log(response.data)
        cookies.set('verificacion', response.verificacion, {path: "/"});
        window.location.href = "./";
    })
    .catch(error=>{
        console.log(error.response.data);
    })
  }
  crearHuesped=async(dUsuario)=>{
    await this.llenarForm();
    await axios.post(url, fd)
    .then(response=>{
        this.setState({dataHuesped: response.data})        
    })
    .catch(error=>{
        console.log(error.response.data);
    })
    this.modificarUsuario(this.state.dataHuesped,dUsuario);        
  }
  addDatos=async()=>{    
    await axios.get(url1)
    .then(response=>{
      this.setState({dataUsuario: response.data})
    }).catch(error=>{
      console.log(error);
    })
  }
  componentDidMount(){
    this.addDatos();
  }
  render(){
    return (
      <div className="c-app c-default-layout flex-row align-items-center">
        <CRow></CRow>
        <CContainer>
          <CRow className="justify-content-center">
            <CCol md="9" lg="8" xl="6">
              <CCard className="mx-4">
                <CCardBody className="p-5">
                  <CForm>
                    <CCol>
                      <h2 class="text-center">Ingrese sus Informacion</h2>
                    </CCol>
                    <CBreadcrumb>
                        <CBreadcrumbItem active></CBreadcrumbItem>
                    </CBreadcrumb>
                    <CFormGroup row>
                      <CCol md="3">
                        <CLabel htmlFor="text-input">Nombre(s)</CLabel>
                      </CCol>
                      <CCol xs="12" md="9">
                        <CInput name="nombre" placeholder="Nombre" onChange={this.handleChange}/>
                      </CCol>
                    </CFormGroup>
                    <CFormGroup row>
                      <CCol md="3">
                        <CLabel htmlFor="text-input">Apellido(s)</CLabel>
                      </CCol>
                      <CCol xs="12" md="9">
                        <CInput name="apellido" placeholder="Apellido" onChange={this.handleChange}/>
                      </CCol>
                    </CFormGroup>
                    <CFormGroup row>
                      <CCol md="3">
                        <CLabel htmlFor="text-input">Grado</CLabel>
                      </CCol>
                      <CCol xs="12" md="9">
                        <CSelect custom name="grado" onChange={this.handleChange}>
                          <option value="Ninguno">Seleccionar grado</option>
                          <option value="SubOficial">SubOficial</option>
                          <option value="Sargento">Sargento</option>
                          <option value="Otro">Otro</option>
                        </CSelect>
                      </CCol>
                    </CFormGroup>
                    <CFormGroup row>
                      <CCol md="3">
                        <CLabel htmlFor="select">Genero</CLabel>
                      </CCol>
                      <CCol xs="12" md="9">
                        <CSelect custom name="genero" onChange={this.handleChange}>
                          <option value="Ninguno">Seleccionar genero</option>
                          <option value="Masculino">Masculino</option>
                          <option value="Femenino">Femenino</option>
                        </CSelect>
                      </CCol>
                    </CFormGroup>
                    <CFormGroup row>
                      <CCol md="3">
                        <CLabel htmlFor="text-input">C.I.</CLabel>
                      </CCol>
                      <CCol xs="12" md="9">
                        <CInput name="carnetIdentidad" placeholder="Carnet de identidad" onChange={this.handleChange}/>
                      </CCol>
                    </CFormGroup>
                    <CFormGroup row>
                      <CCol md="3">
                        <CLabel htmlFor="text-input">C.M.</CLabel>
                      </CCol>
                      <CCol xs="12" md="9">
                        <CInput name="carnetMilitar" placeholder="Carnet Militar" onChange={this.handleChange}/>
                      </CCol>
                    </CFormGroup>                
                    <CBreadcrumb>
                        <CBreadcrumbItem active>Documentos</CBreadcrumbItem>
                    </CBreadcrumb>
                    <CFormGroup>
                      <h6> Ingrese los siguientes documentos escaneados en formato pdf</h6>
                    </CFormGroup>
                    <div className="col justify-content-md-center">
                      <CFormGroup >
                        <CLabel col md="12"><CIcon name="cil-check-circle"/> Carnet de Identidad</CLabel>               
                      </CFormGroup> 
                      <CFormGroup >
                        <CLabel col md="12"><CIcon name="cil-check-circle"/> Carnet Militar</CLabel>
                      </CFormGroup> 
                    </div>            
                    <CFormGroup row>
                      <CLabel col md={3}>Archivos</CLabel>
                      <CCol xs="12" md="8">
                        <CInputFile custom name='documento' onChange={this.handleChangeFile}/>
                        <CLabel htmlFor="custom-file-input" variant="custom-file">
                          Seleccionar archivo...
                        </CLabel>
                      </CCol>
                    </CFormGroup>
                    <CBreadcrumb>
                        <CBreadcrumbItem active></CBreadcrumbItem>
                    </CBreadcrumb>
                    <CButton color="success" block onClick={()=>this.crearHuesped(this.state.dataUsuario)}>Guardar</CButton>                    
                  </CForm>
                </CCardBody>
              </CCard>
            </CCol>
          </CRow>
        </CContainer>
      </div>
    )
  }
}

export default Register