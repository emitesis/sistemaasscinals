import React, {Component} from 'react'
import {
  CAlert,
  CButton,
  CBreadcrumb,
  CBreadcrumbItem,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import axios from 'axios'
import Cookies from 'universal-cookie'
import md5 from 'md5'

const url="http://192.168.0.79:8000/usuario/crear/";
const cookies = new Cookies();

class Register extends Component{

  state={
    visible1: false,
    visible2: false,
    visible3: false,
    msjAlerta:'',
    form:{
      nombreUsuario:'',
      contraseña:'',
      reContraseña:'',
      email:'',
      privilegio:'0',
      verificacion: 'sin.verificar',
      estado: 'true',
      idUsuario: 'x'
    }
  };

  handleChange = async (e) =>{
    await this.setState({
      form:{
        ...this.state.form,
        [e.target.name]: e.target.value
      }
    });
    this.setState({visible1: false});
    this.setState({visible2: false});
    console.log(this.state);
  }

  contraseñaHandleChange = async (e) =>{
    await this.setState({
      form:{
        ...this.state.form,
        contraseña: md5(e.target.value)
      }
    });
    console.log(this.state.form);
  }

  reContraseñaHandleChange = async (e) =>{
    await this.setState({
      form:{
        ...this.state.form,
        reContraseña: md5(e.target.value)
      }
    });
    console.log(this.state.form);
  }

  alert1Visible= ()=>{
    this.setState({visible1: !this.setState.visible1});
  }

  alert2Visible= ()=>{
    this.setState({visible2: !this.setState.visible2});
  }

  alert3Visible= (msj)=>{
    this.setState({visible3: !this.setState.visible3});
    this.setState({msjAlerta: msj});
  }

  registrarUsuario=async()=>{
    let noValido = /\s/;
    let emailNoValido = /^([\da-z_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/
    if(noValido.test(this.state.form.nombreUsuario)){
      this.alert3Visible('El nombre de usuario no debe contener espacios.')
    }else{
      if(!emailNoValido.exec(this.state.form.email))
      {
        this.alert3Visible('Correo electronico no valido')
      }else{
        if(this.state.form.contraseña===this.state.form.reContraseña){
          if(this.state.form.contraseña.length>5){
            await axios.post(url, this.state.form)
            .then(response=>{
                cookies.set('id', response.data.id, {path: "/"});
                cookies.set('nombreUsuario', response.data.nombreUsuario, {path: "/"});
                cookies.set('contraseña', response.data.contraseña, {path: "/"});
                cookies.set('email', response.data.email, {path: "/"});
                cookies.set('privilegio', response.data.privilegio, {path: "/"});
                cookies.set('verificacion', response.data.verificacion, {path: "/"});
                cookies.set('estado', response.data.estado, {path: "/"});
                cookies.set('idPersona', response.data.idUsuario, {path: "/"});
                window.location.href = "./registerData";
            })
            .catch(error=>{
                this.alert1Visible()
                console.log(error.response.data);
            })
          }else{
            this.alert3Visible('Las contraseñas debe tener minimo 6 caracteres.')
          }      
        }else{
          this.alert2Visible()
        }
      }
    }
  }

  componentDidMount() {
    if(cookies.get('nombreUsuario')){
        window.location.href = "./login"
    }
  }

  render(){
    return (
      <div className="c-app c-default-layout flex-row align-items-center">
        <CContainer>
          <CRow className="justify-content-center">
            <CCol md="8" lg="7" xl="5">
              <CCard className="mx-4">
                <CCardBody className="p-5">
                  <CForm>
                    <CCol>
                      <h1 class="text-center">Crea tu cuenta</h1>
                    </CCol>
                    <CBreadcrumb>
                        <CBreadcrumbItem active></CBreadcrumbItem>
                    </CBreadcrumb>
                    <CInputGroup className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput type="text" name="nombreUsuario" onChange={this.handleChange} placeholder="Usuario" autoComplete="NombreUsuario" />
                    </CInputGroup>
                    <CInputGroup className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>@</CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput type="text" name="email" onChange={this.handleChange} placeholder="Email" autoComplete="Email" />
                    </CInputGroup>
                    <CInputGroup className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-lock-locked" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput type="password" name="contraseña" onChange={this.contraseñaHandleChange} placeholder="Contraseña" autoComplete="Contraseña" />
                    </CInputGroup>
                    <CInputGroup className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-lock-locked" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput 
                        type="password" 
                        name="reContraseña" 
                        onChange={this.reContraseñaHandleChange} 
                        placeholder="Repetir Contraseña" 
                        autoComplete="ReContraseña" />
                    </CInputGroup> 
                    <CAlert
                      color="info"
                      show={this.state.visible1}
                      closeButton
                    >   Usuario no disponible.
                    </CAlert>
                    <CAlert
                      color="info"
                      show={this.state.visible2}
                      closeButton
                    >   Las contraseñas no coinsiden.
                    </CAlert>
                    <CAlert
                      color="info"
                      show={this.state.visible3}
                      closeButton
                    >  {this.state.msjAlerta}
                    </CAlert>                           
                    <CButton color="success" onClick={this.registrarUsuario} block>Crear Cuenta</CButton>
                    <p/>
                    <p className="text-center" >
                    Ya tienes una cuenta?
                    <a data-qa-selector="register_link" href="./login"> Iniciar sesion</a>
                    </p>
                  </CForm>
                </CCardBody>
              </CCard>
            </CCol>
          </CRow>
        </CContainer>
      </div>
    )
  }
}

export default Register
