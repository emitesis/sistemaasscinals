import React, {Component} from 'react'
import {
  CBreadcrumb,
  CBreadcrumbItem,
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CFormGroup,
  CInput,
  CLabel,
  CRow
} from '@coreui/react'
import axios from 'axios'
import Cookies from 'universal-cookie'
const cookies = new Cookies();
const url="http://192.168.0.79:8000/usuario/empleado/crear/";
const url1="http://192.168.0.79:8000/usuario/detalle/";
const url2="http://192.168.0.79:8000/usuario/modificar/";

class RegisterEmpleado extends Component {
  state={  
    dataEmpleado:[],
    dataUsuario:[],
    formUsuario:{ 
      nombreUsuario:'',
      contraseña:'',
      email:'',
      privilegio:'',
      verificacion:'',
      estado:'',
      idUsuario:''
    },
    form:{ 
      nombreE:'',
      apellidoE:'',
      cargoE:'XXX',
      carnetIdentidadE:'',
      idUsuarioE:cookies.get('id')
    }
  };
  handleChange = async (e) =>{
    await this.setState({
        form:{
            ...this.state.form,
            [e.target.name]: e.target.value
        }
    });
    console.log(this.state.form)
  }
  modificarUsuario=async(dHuesped,dUser)=>{
    await this.setState({
        formUsuario:{
            ...this.state.formUsuario,
            nombreUsuario:dUser.nombreUsuario,
            contraseña:dUser.contraseña,
            email:dUser.email,
            privilegio:dUser.privilegio,
            verificacion:'verificado',
            estado: true,
            idUsuario:dHuesped.id
        }
    });
    await axios.post(url2+dUser.id+'/', this.state.formUsuario)
    .then(response=>{
        console.log(response.data)
        cookies.set('verificacion', response.verificacion, {path: "/"});
        window.location.href = "./";
    })
    .catch(error=>{
        console.log(error.response.data);
    })
  }
  crearEmpleado=async(dUsuario)=>{
    await axios.post(url, this.state.form)
    .then(response=>{
        this.setState({dataEmpleado: response.data})    
    })
    .catch(error=>{
        console.log(error.response.data);
    })
    this.modificarUsuario(this.state.dataEmpleado,dUsuario);        
  }
  addDatos=async()=>{    
    await axios.get(url1+cookies.get('id'))
    .then(response=>{
      this.setState({dataUsuario: response.data})
    }).catch(error=>{
      console.log(error);
    })
  }
  componentDidMount(){
    this.addDatos();
  }
  render(){
    return (
      <div className="c-app c-default-layout flex-row align-items-center">
        <CRow></CRow>
        <CContainer>
          <CRow className="justify-content-center">
            <CCol md="6" lg="8" xl="5">
              <CCard className="mx-4">
                <CCardBody className="p-5">
                  <CForm>
                    <CCol>
                      <h2 class="text-center">Ingrese su informacion personal</h2>
                    </CCol>
                    <CBreadcrumb>
                        <CBreadcrumbItem active></CBreadcrumbItem>
                    </CBreadcrumb>
                    <CFormGroup row>
                      <CCol md="3">
                        <CLabel htmlFor="text-input">Nombre(s)</CLabel>
                      </CCol>
                      <CCol xs="12" md="9">
                        <CInput name="nombreE" placeholder="Nombre" onChange={this.handleChange}/>
                      </CCol>
                    </CFormGroup>
                    <CFormGroup row>
                      <CCol md="3">
                        <CLabel htmlFor="text-input">Apellido(s)</CLabel>
                      </CCol>
                      <CCol xs="12" md="9">
                        <CInput name="apellidoE" placeholder="Apellido" onChange={this.handleChange}/>
                      </CCol>
                    </CFormGroup>
                    <CFormGroup row>
                      <CCol md="3">
                        <CLabel htmlFor="text-input">C.I.</CLabel>
                      </CCol>
                      <CCol xs="12" md="9">
                        <CInput name="carnetIdentidadE" placeholder="Carnet de idedentidad" onChange={this.handleChange}/>
                      </CCol>
                    </CFormGroup>                  
                    <CBreadcrumb>
                        <CBreadcrumbItem active></CBreadcrumbItem>
                    </CBreadcrumb>
                    <CButton color="success" block onClick={()=>this.crearEmpleado(this.state.dataUsuario)}>Guardar</CButton>                    
                  </CForm>
                </CCardBody>
              </CCard>
            </CCol>
          </CRow>
        </CContainer>
      </div>
    )
  }
}

export default RegisterEmpleado