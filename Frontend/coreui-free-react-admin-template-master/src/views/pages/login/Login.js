import React, {Component} from 'react'
import {
  CAlert,
  CButton,
  CBreadcrumb,
  CBreadcrumbItem,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import axios from 'axios'
import Cookies from 'universal-cookie'
import md5 from 'md5'

const url="http://192.168.0.79:8000/usuario/login/";
const cookies = new Cookies();

class Register extends Component{
  
  constructor(){
    super();
    this.state={
        visible1: false,
        visible2: false,
        nombreUsuario:'',
        contraseña: ''
    };
    this.handleChange=this.handleChange.bind(this);
  }
  
  handleChange = async (e) =>{
    await this.setState({
      [e.target.name]:e.target.value
    });
    this.setState({visible1: false});
    this.setState({visible2: false});
    console.log(this.state);
  }

  alert1Visible= ()=>{
    this.setState({visible1: !this.setState.visible1});
  }

  alert2Visible= ()=>{
    this.setState({visible2: !this.setState.visible2});
  }

  iniciarSesion=async()=>{
    await axios.get(url , {params: {contraseña: md5(this.state.contraseña), nombreUsuario: this.state.nombreUsuario}})
    .then(response=>{
      return response.data;
    })
    .then(response=>{
        if(response.length>0){
            var respuesta=response[0];
            if(respuesta.estado===true){
              if(respuesta.nombreUsuario===this.state.nombreUsuario&&respuesta.contraseña===md5(this.state.contraseña)){
                cookies.set('id', respuesta.id, {path: "/"});
                cookies.set('nombreUsuario', respuesta.nombreUsuario, {path: "/"});
                cookies.set('privilegio', respuesta.privilegio, {path: "/"});
                cookies.set('verificacion', respuesta.verificacion, {path: "/"});
                cookies.set('estado', respuesta.estado, {path: "/"});
                cookies.set('idPersona', respuesta.idUsuario, {path: "/"});
                if(respuesta.verificacion==='sin.verificar'){
                  if(respuesta.privilegio===0){
                    window.location.href = "./registerData";                  
                  }else{
                    window.location.href = "./register/empleado"; 
                  }
                }else{
                  window.location.href = "./";
                }
              }else{
                this.alert1Visible()
              }
            }else{
              this.alert2Visible()
            }
        }else{
          this.alert1Visible()
        }
    })
    .catch(error=>{
        console.log(error);
    })
  }
  componentDidMount() {
    if(cookies.get('nombreUsuario')){
      if(cookies.get('verificacion')==='sin.verificar'){
        if(cookies.get('privilegio')==='0'){
          window.location.href = "./registerData";                  
        }else{
          window.location.href = "./register/empleado"; 
        }
      }else{
        window.location.href = "./"
      }
    }
  }
  render(){
    return (
      <div className="c-app c-default-layout flex-row align-items-center">
        <CContainer>
          <CRow className="justify-content-center">
            <CCol md="8" lg="7" xl="5">
              <CCard className="mx-4">
                <CCardBody className="p-5">
                  <CForm>
                    <CCol>
                      <h1 class="text-center">Bienvenido</h1>
                    </CCol>
                    <CBreadcrumb>
                        <CBreadcrumbItem active></CBreadcrumbItem>
                    </CBreadcrumb>
                    <CInputGroup className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput type="text" name="nombreUsuario" placeholder="Usuario" onChange={this.handleChange} value={this.state.nombreUsuario}/>
                    </CInputGroup>
                    <CInputGroup className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-lock-locked" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput type="password" name="contraseña" placeholder="Contraseña" onChange={this.handleChange}  value={this.state.contraseña} />
                    </CInputGroup>
                    <CInputGroup>
                      <CAlert
                        color="info"
                        show={this.state.visible1}
                        closeButton
                      >El usuario o la contraseña no son correctos.
                      </CAlert>
                      <CAlert
                        color="info"
                        show={this.state.visible2}
                        closeButton
                      >Usuario Bloqueado.
                      </CAlert>
                    </CInputGroup>            
                    <CButton color="success" block onClick={this.iniciarSesion}>Iniciar sesion</CButton>
                    <p/>
                    <p className="text-center" >
                      ¿No tienes una cuenta?
                      <a data-qa-selector="register_link" href="./register"> Registrate ahora</a>
                    </p>
                  </CForm>
                </CCardBody>
              </CCard>
            </CCol>
          </CRow>
        </CContainer>
      </div>
    )
  }
  
}

export default Register
