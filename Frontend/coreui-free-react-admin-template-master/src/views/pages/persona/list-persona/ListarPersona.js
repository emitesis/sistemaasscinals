import React, { Component } from 'react'
import {
  CButton,
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CForm,
  CFormGroup,
  CInput,
  CLabel,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CRow,
} from '@coreui/react'
import axios from 'axios'
import CIcon from '@coreui/icons-react'
import moment from 'moment';
import Cookies from 'universal-cookie';
const cookies = new Cookies();

const fields = ['nombre','apellido', 'carnetIdentidad', 'verificacion', 'accion']
const url="http://192.168.0.79:8000/usuario/listar/";
const url1="http://192.168.0.79:8000/usuario/huesped/listar/";

class ListPersona extends Component {  
  state={
    alert:false,
    dataUsuarios:[],
    dataHuespedes:[],
    dataPersona:[],
    data:[],
    modal: false,
    modalAntecedente: false,
    modalReserva: false,
    modalEstadia: false,
    msj:'',
    nomHuesped:'',
    form:{
      nombreUsuario:'',
      contraseña:'',
      email:'',
      privilegio:'',
      verificacion: '',
      estado: '',
      idUsuario: ''
    },
    formAntecedente:{
      antecedente:'',
      idTipo:'',
      idHuesped:'',
      estado:'En progreso'
    }
  };

  modalVisible= ()=>{
    this.setState({modal: !this.state.modal});
  }

  handleChange = async (e) =>{
    await this.setState({
        formAntecedente:{
            ...this.state.formAntecedente,
            [e.target.name]: e.target.value
        }
    });
    console.log(this.state.formAntecedente)
    this.setState({alert: false});
  }

  getVerificacion = (status) => {
    switch (status) {
      case 'verificado': return 'Verificado'
      case 'en.progeso': return 'En Progreso'
      default: return 'Sin Verificar'
    }
  }
  getColorVerificacion = (status) => {
    switch (status) {
      case 'verificado': return 'success'
      case 'en.progeso': return 'warning'
      default: return 'danger'
    }
  }
  getEstado = (status) => {
    switch (status) {
      case true: return 'Habilitado'
      default: return 'Bloqueado'
    }
  }
  getColorEstado = (status) => {
    switch (status) {
      case true: return 'success'
      default: return 'danger'
    }
  }
  getColorEstadoA = (status) => {
    switch (status) {
      case 'Aprobado': return 'success'
      case 'En progreso': return 'warning'
      default: return 'danger'
    }
  }
  alertVisible= (e)=>{
    this.setState({alert: !this.state.alert});
    this.setState({msj: e});
  }

  addDatosPersona=async()=>{    
    await axios.get(url1)
    .then(response=>{
      this.setState({dataPersona: response.data})
      let result = this.state.dataUsuarios.map(v => ({ ...v, ...this.state.dataPersona.find(sp => sp.id === parseInt(v.idUsuario))}));
      this.setState({dataHuespedes: result})
    }).catch(error=>{
      console.log(error);
    })
  }

  addDatos=async()=>{    
    await axios.get(url)
    .then(response=>{
      let output = response.data.filter((obj) => obj.privilegio === 0 && obj.verificacion!=='en.progreso');
      this.setState({dataUsuarios: output})
    }).catch(error=>{
      console.log(error);
    })
    this.addDatosPersona();
  }

  addFormHuesped=async(e)=>{
    await this.setState({data: e})
    this.modalVisible()
  }

  componentDidMount(){
    this.addDatos();
  }
  render(){
    return (
      <CRow>
        <CCol>
          <CCard>
            <CCardHeader>
              Huespedes
            </CCardHeader>
            <CCardBody>
            <CModal 
              show={this.state.modal} 
              onClose={this.modalVisible}
              color="info"
              >
                  <CModalHeader closeButton>
                      <CModalTitle>Datos de Huesped</CModalTitle>
                  </CModalHeader>
                  <CModalBody>
                      <CCard>
                          <CCardBody>
                              <CForm action="" encType="multipart/form-data" className="form-horizontal">
                                  <CFormGroup row>
                                    <CCol md="4">
                                        <CLabel htmlFor="text-input">Nombre(s)</CLabel>
                                    </CCol>
                                    <CCol xs="12" md="8">
                                        <CInput id="Nombre" name="nombre" value={this.state.data.nombre}/>
                                    </CCol>
                                  </CFormGroup>
                                  <CFormGroup row>
                                  <CCol md="4">
                                      <CLabel htmlFor="text-input">Apellido(s)</CLabel>
                                  </CCol>
                                  <CCol xs="12" md="8">
                                      <CInput name="apellido" value={this.state.data.apellido}/>
                                  </CCol>
                                  </CFormGroup>
                                  <CFormGroup row>
                                  <CCol md="4">
                                      <CLabel htmlFor="text-input">Grado</CLabel>
                                  </CCol>
                                  <CCol xs="12" md="8">
                                      <CInput name="grado" value={this.state.data.grado}/>
                                  </CCol>
                                  </CFormGroup>
                                  <CFormGroup row>
                                  <CCol md="4">
                                      <CLabel htmlFor="text-input">Genero</CLabel>
                                  </CCol>
                                  <CCol xs="12" md="8">
                                      <CInput name="genero" value={this.state.data.genero}/>
                                  </CCol>
                                  </CFormGroup>
                                  <CFormGroup row>
                                  <CCol md="4">
                                      <CLabel htmlFor="text-input">Carnet de Identidad</CLabel>
                                  </CCol>
                                  <CCol xs="12" md="8">
                                      <CInput name="carnetIdentidad" value={this.state.data.carnetIdentidad}/>
                                  </CCol>
                                  </CFormGroup>
                                  <CFormGroup row>
                                  <CCol md="4">
                                      <CLabel htmlFor="text-input">Carnet Militar</CLabel>
                                  </CCol>
                                  <CCol xs="12" md="8">
                                      <CInput name="carnetMilitar" value={this.state.data.carnetMilitar}/>
                                  </CCol>
                                  </CFormGroup>
                                  <CFormGroup row>
                                  <CCol md="4">
                                      <CLabel htmlFor="text-input">Nombre de Usuario</CLabel>
                                  </CCol>
                                  <CCol xs="12" md="8">
                                      <CInput name="nombreUsuario" value={'@'+this.state.data.nombreUsuario}/>
                                  </CCol>
                                  </CFormGroup>
                                  <CFormGroup row>
                                  <CCol md="4">
                                      <CLabel htmlFor="text-input">Email</CLabel>
                                  </CCol>
                                  <CCol xs="12" md="8">
                                      <CInput name="email" value={this.state.data.email}/>
                                  </CCol>
                                  </CFormGroup>
                                  <CFormGroup row>
                                  <CCol md="4">
                                      <CLabel htmlFor="text-input">Verificacion</CLabel>
                                  </CCol>
                                  <CCol xs="12" md="8">
                                    <CBadge color={this.getColorVerificacion(this.state.data.verificacion)}>{this.getVerificacion(this.state.data.verificacion)}</CBadge>
                                  </CCol>
                                  </CFormGroup>
                                  <CFormGroup row>
                                  <CCol md="4">
                                      <CLabel htmlFor="text-input">Estado</CLabel>
                                  </CCol>
                                  <CCol xs="12" md="8">
                                    <CBadge color={this.getColorEstado(this.state.data.estado)}>{this.getEstado(this.state.data.estado)}</CBadge>
                                  </CCol>
                                  </CFormGroup>
                              </CForm>
                          </CCardBody>
                      </CCard>
                  </CModalBody>
                  <CModalFooter>
                      <CButton variant="outline" color="danger" onClick={()=>this.modalVisible()}><CIcon name="cil-account-logout"/>{' '}Volver</CButton>
                  </CModalFooter>
                </CModal>
            <hr/>
              <CDataTable
                    items={this.state.dataHuespedes}
                    fields={fields}
                    itemsPerPage={5}
                    pagination
                    footer
                    tableFilter
                    scopedSlots = {{
                      'nombre':
                      (item)=>(
                          <td>
                              {item.nombre.charAt(0).toUpperCase() + item.nombre.slice(1)}
                          </td>
                      ),
                      'apellido':
                        (item)=>(
                            <td>
                                {item.apellido.charAt(0).toUpperCase() + item.apellido.slice(1)}
                            </td>
                      ),
                        'carnetIdentidad':
                        (item)=>(
                            <td>
                                {item.carnetIdentidad}
                            </td>
                        ),
                        'verificacion':
                        (item)=>(
                            <td>
                            <CBadge color={this.getColorVerificacion(item.verificacion)}>
                                {this.getVerificacion(item.verificacion)}
                            </CBadge>
                            </td>
                        ),
                        'accion':
                        (item)=>(
                            <td>
                              <CButton variant="outline" color="info" size="sm" onClick={()=>this.addFormHuesped(item)}><CIcon name="cil-address-book"/>{' '}Ver Datos</CButton>{' '}
                            </td>
                        )

                    }}
                />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    )
  }  
}

export default ListPersona