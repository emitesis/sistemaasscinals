import React, { Component } from 'react'
import {
  CButton,
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CForm,
  CFormGroup,
  CInput,
  CLabel,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CRow
} from '@coreui/react'
import axios from 'axios'
import CIcon from '@coreui/icons-react'
import Cookies from 'universal-cookie';
const cookies = new Cookies();

const fields = ['nombreUsuario', 'fechaCreacion', 'estado', 'accion']
const url="http://192.168.0.79:8000/usuario/listar/";
const url1="http://192.168.0.79:8000/usuario/huesped/detalle/";
const url2="http://192.168.0.79:8000/usuario/modificar/";


class VerificarPersona extends Component {

  
  state={
    dataUsuarios:[],
    dataUsuario:[],
    dataPersona:[],
    modal: false,
    form:{
      nombreUsuario:'',
      contraseña:'',
      email:'',
      privilegio:'',
      verificacion: '',
      estado: '',
      idUsuario: ''
    }
  };

  modalVisible= ()=>{
    this.setState({modal: !this.state.modal});
  }

  addDatos=async()=>{    
    await axios.get(url)
    .then(response=>{
      let outputProgress = response.data.filter((obj) => obj.privilegio === 0&&obj.verificacion==='en.progreso');
      this.setState({dataUsuarios: outputProgress})
      console.log(this.state.dataUsuario)
    }).catch(error=>{
      console.log(error);
    })
  }

  addDatosForm=async(e)=>{
    await this.setState({
      form:{
        ...this.state.form,
        nombreUsuario: e.nombreUsuario,
        contraseña: e.contraseña,
        email: e.email,
        privilegio: e.privilegio,
        verificacion: 'verificado',
        estado: e.estado,
        idUsuario: e.idUsuario
      }
    });
  }

  addDatosFormBloqueado=async(e)=>{
    await this.setState({
      form:{
        ...this.state.form,
        nombreUsuario: e.nombreUsuario,
        contraseña: e.contraseña,
        email: e.email,
        privilegio: e.privilegio,
        verificacion: 'verificado',
        estado: !e.estado,
        idUsuario: e.idUsuario
      }
    });
  }

  verificarDatosUsuario=async()=>{
    await this.addDatosForm(this.state.dataUsuario)
    axios.post(url2+this.state.dataUsuario.id+"/", this.state.form)
    .then(response=>{
        console.log(response)
        this.addDatos();
        this.modalVisible()
    })
    .catch(error=>{
        console.log(error.response.data);
    })
  }

  bloquearDatosUsuario=async()=>{ 
    await this.addDatosFormBloqueado(this.state.dataUsuario)   
    await axios.post(url2+this.state.dataUsuario.id+"/", this.state.form)
    .then(response=>{
        console.log(response)
        this.addDatos();
        this.modalVisible()
    })
    .catch(error=>{
        console.log(error.response.data);
    })
  }

  addDatosPersona=async(e)=>{    
    await axios.get(url1+e.idUsuario+'/')
    .then(response=>{
      this.setState({dataPersona: response.data})
      this.setState({dataUsuario: e})
    }).catch(error=>{
      console.log(error);
    })
    this.modalVisible()
  }

  componentDidMount(){
    this.addDatos();
  }
  render(){
    return (
      <CRow>
        <CCol>
          <CCard>
            <CCardHeader>
              Huespedes no verificados
            </CCardHeader>
            <CCardBody>
            <CModal 
              show={this.state.modal} 
              onClose={this.modalVisible}
              color="info"
              >
                  <CModalHeader closeButton>
                      <CModalTitle>Datos de Huesped</CModalTitle>
                  </CModalHeader>
                  <CModalBody>
                      <CCard>
                          <CCardBody>
                              <CForm action="" encType="multipart/form-data" className="form-horizontal">
                                  <CFormGroup row>
                                    <CCol md="4">
                                        <CLabel htmlFor="text-input">Nombre(s)</CLabel>
                                    </CCol>
                                    <CCol xs="12" md="8">
                                        <CInput id="Nombre" name="nombre" value={this.state.dataPersona.nombre}/>
                                    </CCol>
                                  </CFormGroup>
                                  <CFormGroup row>
                                  <CCol md="4">
                                      <CLabel htmlFor="text-input">Apellido(s)</CLabel>
                                  </CCol>
                                  <CCol xs="12" md="8">
                                      <CInput name="apellido" value={this.state.dataPersona.apellido}/>
                                  </CCol>
                                  </CFormGroup>
                                  <CFormGroup row>
                                  <CCol md="4">
                                      <CLabel htmlFor="text-input">Grado</CLabel>
                                  </CCol>
                                  <CCol xs="12" md="8">
                                      <CInput name="grado" value={this.state.dataPersona.grado}/>
                                  </CCol>
                                  </CFormGroup>
                                  <CFormGroup row>
                                  <CCol md="4">
                                      <CLabel htmlFor="text-input">Genero</CLabel>
                                  </CCol>
                                  <CCol xs="12" md="8">
                                      <CInput name="genero" value={this.state.dataPersona.genero}/>
                                  </CCol>
                                  </CFormGroup>
                                  <CFormGroup row>
                                  <CCol md="4">
                                      <CLabel htmlFor="text-input">Carnet de Identidad</CLabel>
                                  </CCol>
                                  <CCol xs="12" md="8">
                                      <CInput name="carnetIdentidad" value={this.state.dataPersona.carnetIdentidad}/>
                                  </CCol>
                                  </CFormGroup>
                                  <CFormGroup row>
                                  <CCol md="4">
                                      <CLabel htmlFor="text-input">Carnet Militar</CLabel>
                                  </CCol>
                                  <CCol xs="12" md="8">
                                      <CInput name="carnetMilitar" value={this.state.dataPersona.carnetMilitar}/>
                                  </CCol>
                                  </CFormGroup>
                                  <CFormGroup row>
                                  <CCol md="4">
                                      <CLabel htmlFor="text-input">Documentos PDF</CLabel>
                                  </CCol>
                                  <CCol col="2" className="text-center mt-3">
                                    <a href={'http://192.168.0.79:8000'+this.state.dataPersona.documento} target="_blank">
                                      <CButton block variant="ghost" color="success">
                                        <CIcon name="cil-fingerprint" />{' '}Descargar documentos
                                      </CButton>
                                    </a>
                                  </CCol>
                                  </CFormGroup>
                              </CForm>
                          </CCardBody>
                      </CCard>
                  </CModalBody>
                  <CModalFooter>
                      <CButton variant="outline" color="info" onClick={()=>this.verificarDatosUsuario()}><CIcon name="cil-user-follow"/>{' '}Verificar</CButton>{' '}
                      <CButton variant="outline" color="danger" onClick={()=>this.bloquearDatosUsuario()}><CIcon name="cil-user-unfollow"/>{' '}Bloquear</CButton>{' '}
                      <CButton variant="outline" color="warning" onClick={()=>this.modalVisible()}><CIcon name="cil-account-logout"/>{' '}Volver</CButton>
                  </CModalFooter>
                </CModal>
                <CDataTable
                    items={this.state.dataUsuarios}
                    fields={fields}
                    itemsPerPage={5}
                    pagination
                    footer
                    tableFilter
                    scopedSlots = {{
                      'nombreUsuario':
                        (item)=>(
                            <td>
                                @{item.nombreUsuario}
                            </td>
                        ),
                      'fechaCreacion':
                        (item)=>(
                            <td>
                              {item.created_at}
                            </td>
                        ),
                      'estado':
                        (item)=>(
                            <td>
                              <CBadge color='warning'>
                                {item.verificacion}
                              </CBadge>
                            </td>
                        ),                      
                      'accion':
                        (item)=>(
                            <td>
                               <CButton variant="outline" color="info" size="sm" onClick={(e)=>this.addDatosPersona(item)}>
                                  <CIcon name="cil-address-book"/>{' '}Ver Datos
                               </CButton>                      
                            </td>
                        )
                    }}
                />
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    )
  }  
}

export default VerificarPersona