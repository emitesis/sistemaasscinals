import React from 'react'
import {
  CCard,
  CCardBody,
  CCol,
  CRow
} from '@coreui/react'

const Dashboard = () => {
  return (
    <>
      <CRow>
        <CCol>
          <CCard>
            <CCardBody>
              <CRow className="justify-content-center">
                <h1>ASSCINALS</h1>
              </CRow> 
              <hr/>
              <CRow>
                <CCol xs="12" md="4" >
                    <CCard>
                        <CCardBody>
                        <CRow className="justify-content-center">
                            <img src="https://i.ibb.co/3FnrVRQ/Ejercito-EB.png" width="250" height="250" alt="description of image"/>
                        </CRow>
                        </CCardBody>
                    </CCard>                    
                </CCol>
                <CCol xs="12" md="4" >
                    <CCard>
                        <CCardBody>
                        <CRow className="justify-content-center">
                            <img src="https://i.ibb.co/4sxSf7C/ArmadaAB.png" width="250" height="250" alt="description of image"/>
                        </CRow>
                        </CCardBody>
                    </CCard>                    
                </CCol>
                <CCol xs="12" md="4" >
                    <CCard>
                        <CCardBody>
                        <CRow className="justify-content-center">
                            <img src="https://i.ibb.co/pzdWQCm/Escudo-de-armas-o-emblema-de-la-FAB.jpg" width="250" height="250" alt="description of image"/>
                        </CRow>
                        </CCardBody>
                    </CCard>                    
                </CCol>
              </CRow>
              <hr/>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  )
}

export default Dashboard