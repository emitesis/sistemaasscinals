const navegateSecretaria =  [
    {
      _tag: 'CSidebarNavItem',
      name: 'Inicio',
      to: '/',
      icon: 'cil-home',
    },
    {
      _tag: 'CSidebarNavDropdown',
      name: 'Huesped',
      route: '/buttons',
      icon: 'cil-user',
      _children: [
        {
          _tag: 'CSidebarNavItem',
          name: 'Listar',
          to: '/huesped/Listar',
        },
        {
          _tag: 'CSidebarNavItem',
          name: 'Verificar',
          to: '/huesped/verificar',
        },
      ],
    }, 
  ]
  
  export default navegateSecretaria