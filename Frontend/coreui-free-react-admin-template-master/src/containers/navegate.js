
const _nav =  [
  {
    _tag: 'CSidebarNavItem',
    name: 'Inicio',
    to: '/',
    icon: 'cil-home',
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Huesped',
    route: '/buttons',
    icon: 'cil-user',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Listar',
        to: '/huesped/Listar',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Verificar',
        to: '/huesped/verificar',
      },
    ],
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Habitacion',
    route: '/base',
    icon: 'cil-bed',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Habitacion Disponible',
        to: '/habitacion/listar',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Tipo de Habitacion',
        to: '/habitacion/tipo',
      },
    ],
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Estadia',
    route: '/icons',
    icon: 'cil-calendar',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Registrar',
        to: '/estadia/registrar',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Cerrar',
        to: '/estadia/listar',
      },      
      {
        _tag: 'CSidebarNavItem',
        name: 'Listar',
        to: '/estadia/huesped',
      },
    ],
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Reserva',
    route: '/icons',
    icon: 'cil-pencil',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Registrar Reserva',
        to: '/reserva/registrar',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Seguimiento de reservas',
        to: '/reserva/evaluacion',
      },
    ],
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Antecedentes',
    route: '/icons',
    icon: 'cil-folder-open',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Registrar',
        to: '/antecedente/registrar',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Aprobar',
        to: '/antecedente/aprobar',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Listar',
        to: '/antecedente/huesped',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Tipo de Antecedente',
        to: '/antecedente/tipo',
      },
    ],
  },  
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Usuarios',
    route: '/icons',
    icon: 'cil-user-follow',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Lista de Usuario',
        to: '/usuario',
      },
    ],
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Empleado',
    route: '/icons',
    icon: 'cil-people',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Lista de empleado',
        to: '/empleado',
      },
    ],
  },
  
]

export default _nav
