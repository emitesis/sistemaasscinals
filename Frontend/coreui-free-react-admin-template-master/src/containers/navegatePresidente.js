const navegatePresidente =  [
    {
      _tag: 'CSidebarNavItem',
      name: 'Inicio',
      to: '/',
      icon: 'cil-home',
    },
    {
      _tag: 'CSidebarNavDropdown',
      name: 'Usuarios',
      route: '/icons',
      icon: 'cil-user-follow',
      _children: [
        {
          _tag: 'CSidebarNavItem',
          name: 'Lista de Usuario',
          to: '/usuario',
        },
      ],
    },
    
  ]
  
  export default navegatePresidente