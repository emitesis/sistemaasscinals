import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  CCreateElement,
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
  CSidebarNavDivider,
  CSidebarNavTitle,
  CSidebarMinimizer,
  CSidebarNavDropdown,
  CSidebarNavItem,
} from '@coreui/react'

import CIcon from '@coreui/icons-react'
import logos from '../assets/logo.png'

// sidebar navegate config
//import navigation from './navegate'
import navigationHuesped from './navegateHuesped'
//import nav from './_nav'
import Cookies from 'universal-cookie'
import navegateSecretaria from './navegateSecretaria'
import navegatePresidente from './navegatePresidente'

const cookies = new Cookies();
const privilegio=cookies.get('privilegio')

const TheSidebar = () => {
  const dispatch = useDispatch()
  const show = useSelector(state => state.sidebarShow)

  const getRol = status => {
    switch (status) {
      case '0': return navigationHuesped
      case '1': return navegatePresidente
      default: return navegateSecretaria
    }
  }
  return (
    <CSidebar
      show={show}
      onShowChange={(val) => dispatch({type: 'set', sidebarShow: val })}
    >
      <CSidebarBrand className="d-md-down-none" to="/">
        <CIcon
          className="c-sidebar-brand-full"
          name="logo-negative"
          src={logos}
          height={35}
        />
        <CIcon
          className="c-sidebar-brand-minimized"
          name="sygnet"
          src='https://i.ibb.co/gz30B36/logo-removebg-preview.png'
          height={35}
        />
      </CSidebarBrand>
      <CSidebarNav>

        <CCreateElement
          items={getRol(privilegio)}
          components={{
            CSidebarNavDivider,
            CSidebarNavDropdown,
            CSidebarNavItem,
            CSidebarNavTitle
          }}
        />
      </CSidebarNav>
      <CSidebarMinimizer className="c-d-md-down-none"/>
    </CSidebar>
  )
}

export default React.memo(TheSidebar)
