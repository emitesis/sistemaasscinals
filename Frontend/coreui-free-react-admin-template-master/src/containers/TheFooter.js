import React from 'react'
import { CFooter } from '@coreui/react'

const TheFooter = () => {
  return (
    <CFooter fixed={false}>
      <div>
        <a href="http://www.ascinalss.org/" target="_blank" rel="noopener noreferrer">Ascinalss</a>
        <span className="ml-1">&copy; 2021.</span>
      </div>
      <div className="mfs-auto">
        <span className="mr-1">Cochabamba</span>
      </div>
    </CFooter>
  )
}

export default React.memo(TheFooter)
