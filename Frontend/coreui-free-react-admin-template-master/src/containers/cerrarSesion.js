
const _nav =  [
    {
      _tag: 'CSidebarNavItem',
      name: 'Inicio',
      to: '/theme/colors',
      icon: 'cil-home',
    },
    {
      _tag: 'CSidebarNavDropdown',
      name: 'Habitacion',
      route: '/base',
      icon: 'cil-bed',
      _children: [
        {
          _tag: 'CSidebarNavItem',
          name: 'Habitacion Disponible',
          to: '/base/breadcrumbs',
        },
        {
          _tag: 'CSidebarNavItem',
          name: 'Tipo de Habitacion',
          to: '/base/cards',
        },
      ],
    },
    {
      _tag: 'CSidebarNavDropdown',
      name: 'Huesped',
      route: '/buttons',
      icon: 'cil-user',
      _children: [
        {
          _tag: 'CSidebarNavItem',
          name: 'Aprobacion de Huespedes',
          to: '/buttons/buttons',
        },
        {
          _tag: 'CSidebarNavItem',
          name: 'Listado de Huespedes',
          to: '/buttons/brand-buttons',
        },
        {
          _tag: 'CSidebarNavItem',
          name: 'Buttons groups',
          to: '/buttons/button-groups',
        },
      ],
    },
    {
      _tag: 'CSidebarNavDropdown',
      name: 'Reserva',
      route: '/icons',
      icon: 'cil-pencil',
      _children: [
        {
          _tag: 'CSidebarNavItem',
          name: 'Registrar Reserva',
          to: '/icons/coreui-icons',
        },
        {
          _tag: 'CSidebarNavItem',
          name: 'Listado de Reservas',
          to: '/icons/flags',
        },
        {
          _tag: 'CSidebarNavItem',
          name: 'Seguimiento de reservas',
          to: '/icons/brands',
        },
      ],
    },
    {
      _tag: 'CSidebarNavDropdown',
      name: 'Usuarios',
      route: '/icons',
      icon: 'cil-user-follow',
      _children: [
        {
          _tag: 'CSidebarNavItem',
          name: 'Registro de usuario',
          to: '/icons/coreui-icons',
        },
        {
          _tag: 'CSidebarNavItem',
          name: 'Listado de Usuario',
          to: '/icons/flags',
        },
      ],
    },
    {
      _tag: 'CSidebarNavDropdown',
      name: 'Notifications',
      route: '/notifications',
      icon: 'cil-bell',
      _children: [
        {
          _tag: 'CSidebarNavItem',
          name: 'Alerts',
          to: '/notifications/alerts',
        },
        {
          _tag: 'CSidebarNavItem',
          name: 'Badges',
          to: '/notifications/badges',
        },
        {
          _tag: 'CSidebarNavItem',
          name: 'Modal',
          to: '/notifications/modals',
        },
        {
          _tag: 'CSidebarNavItem',
          name: 'Toaster',
          to: '/notifications/toaster'
        }
      ]
    },
  ]
  
  export default _nav