import React, {Component} from 'react'
import {
  CBadge,
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CImg
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import Cookies from 'universal-cookie'
const cookies = new Cookies();

const verificacion=cookies.get('verificacion')

class TheHeaderDropdown extends Component{
    
  cerrarSesion = () =>{
    cookies.remove('id', {path: "/"});
    cookies.remove('nombreUsuario',{path: "/"});
    cookies.remove('contraseña',{path: "/"});
    cookies.remove('email',{path: "/"});
    cookies.remove('privilegio', {path: "/"});
    cookies.remove('verificacion', {path: "/"});
    cookies.remove('estado',{path: "/"});
    cookies.remove('idPersona',{path: "/"});
    window.location.href = "./login"
  }
  getColorVerificaion = () => {
    switch (verificacion) {
      case 'verificado': return 'success'
      case 'en.progreso': return 'warning'
      default: return 'warning'
    }
  }
  getIconVerificaion = () => {
    switch (verificacion) {
      case 'verificado': return 'cil-check-circle'
      default: return 'cil-x-circle'
    }
  }
  getVerificaion = () => {
    switch (verificacion) {
      case 'verificado': return 'Verificado'
      case 'en.progreso': return 'Sin verificar'
      default: return 'En Progreso'
    }
  }
  render(){
  return (
    <CDropdown
      inNav
      className="c-header-nav-items mx-2"
      direction="down"
    >
      <CDropdownToggle className="c-header-nav-link" caret={false}>
        <div className="c-avatar">
          <CImg
            src={'avatars/user.png'}
            className="c-avatar-img"
            alt="admin@bootstrapmaster.com"
          />
        </div>
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" placement="bottom-end">
        <CDropdownItem
          header
          tag="div"
          color="light"
          className="text-center"
        >
          <strong>Cuenta: {cookies.get('nombreUsuario')}   &nbsp;&nbsp;&nbsp;&nbsp;</strong>
          {'   '}
          <CBadge color={this.getColorVerificaion()}><CIcon name={this.getIconVerificaion()} className="mfe-2" />{this.getVerificaion()}</CBadge>
        </CDropdownItem>       
        <CDropdownItem to="./registrousuario">
          <CIcon name="cil-user" className="mfe-2" />Perfil
        </CDropdownItem>

        <CDropdownItem divider />

        <CDropdownItem onClick={this.cerrarSesion}>
          <CIcon name="cil-lock-locked" className="mfe-2" />
          Cerrar Sesion
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  )
  }
}

export default TheHeaderDropdown
