from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import UsuarioSerializer, EmpleadoSerializer, PersonaSerializer
from rest_framework.parsers import MultiPartParser, FormParser
from .models import Usuario, Empleado, Persona
from rest_framework.decorators import api_view
from rest_framework import status
from django.http import FileResponse
from django.http import HttpResponse



@api_view(['GET'])
def usuarioList(request):
    usuarios = Usuario.objects.all()
    serializer = UsuarioSerializer(usuarios, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def usuarioDetail(request, pk):
    usuarios = Usuario.objects.get(id=pk)
    serializer = UsuarioSerializer(usuarios, many=False)
    return Response(serializer.data)
    
@api_view(['GET'])
def usuarioNoVerificadoList(request):
    usuarios = Usuario.objects.filter(verificacion=False)
    serializer = UsuarioSerializer(usuarios, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def usuarioLogin(request):
    usuarios = Usuario.objects.filter(nombreUsuario__contains=request.GET['nombreUsuario'], contraseña__contains=request.GET['contraseña'])
    serializer = UsuarioSerializer(usuarios, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def usuarioCreate(request):
    serializer = UsuarioSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    else:
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    return Response(serializer.data)

@api_view(['POST'])
def usuarioUpdate(request,pk):
    usuario = Usuario.objects.get(id=pk)
    serializer = UsuarioSerializer(instance=usuario, data=request.data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)

@api_view(['DELETE'])
def usuarioDelete(request, pk):
    usuario = Usuario.objects.get(id=pk)
    usuario.delete()
    return Response('Deleted')

#/**Vista del modelo Empleado**\\

@api_view(['GET'])
def empleadoList(request):
    empleado = Empleado.objects.all()
    serializer = EmpleadoSerializer(empleado, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def empleadoDetail(request, pk):
    empleado = Empleado.objects.get(id=pk)
    serializer = EmpleadoSerializer(empleado, many=False)
    return Response(serializer.data)

@api_view(['POST'])
def empleadoCreate(request):
    serializer = EmpleadoSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    else:
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def empleadoUpdate(request,pk):
    empleado = Empleado.objects.get(id=pk)
    serializer = EmpleadoSerializer(instance=empleado, data=request.data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)

#/**Vista del modelo Huesped**\\

@api_view(['GET'])
def huespedList(request):
    persona = Persona.objects.all()
    serializer = PersonaSerializer(persona, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def huespedDetail(request, pk):
    persona = Persona.objects.get(id=pk)
    serializer = PersonaSerializer(persona, many=False)
    return Response(serializer.data)

@api_view(['GET'])
def download(request, id):
    obj = Persona.objects.get(id=id)
    filename = obj.documento.path
    response = FileResponse(open(filename, 'rb'))
    return response

@api_view(['GET'])
def huespedRegister(request):
    persona = Persona.objects.filter(nombre__icontains=request.GET['nombre'], apellido__icontains=request.GET['apellido'])
    serializer = PersonaSerializer(persona, many=True)
    return Response(serializer.data)

class FileView(APIView):
  parser_classes = (MultiPartParser, FormParser)
  def post(self, request, *args, **kwargs):
    persona_serializer = PersonaSerializer(data=request.data)
    if persona_serializer.is_valid():
        persona_serializer.save()
        return Response(persona_serializer.data, status=status.HTTP_201_CREATED)
    else:
        return Response(persona_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def huespedCreate(request):
    serializer = PersonaSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    else:
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
