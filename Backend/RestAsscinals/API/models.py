from django.db import models

class Usuario(models.Model):
    nombreUsuario = models.CharField(max_length=50, unique=True)
    contraseña = models.CharField(max_length=50)
    email = models.CharField(max_length=100, unique=True)
    privilegio = models.IntegerField()
    verificacion = models.CharField(max_length=50)
    estado = models.BooleanField(default=True)
    idUsuario = models.CharField(max_length=50)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

    class Meta:
            db_table = 'usuario'

class Empleado(models.Model):
    nombreE = models.CharField(max_length=50)
    apellidoE = models.CharField(max_length=50)
    carnetIdentidadE = models.CharField(max_length=50)
    cargoE = models.CharField(max_length=50)
    idUsuarioE = models.CharField(max_length=50)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

    class Meta:
            db_table = 'empleado'

class Persona(models.Model):
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    genero = models.CharField(max_length=50)
    grado = models.CharField(max_length=50,blank=True)
    carnetIdentidad = models.IntegerField(unique=True)
    carnetMilitar = models.IntegerField(unique=True, blank=True)
    documento = models.FileField(null=True, blank=True)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

    class Meta:
            db_table = 'persona'