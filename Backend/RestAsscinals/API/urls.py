from django.urls import path
from .import views


urlpatterns = [
    path('listar/', views.usuarioList,name='listarU'),
    path('detalle/<str:pk>/', views.usuarioDetail, name='detalleU'),
    path('usuario/listar/nverificado', views.usuarioNoVerificadoList,name='listarUNV'),
    path('login/', views.usuarioLogin, name='login'),
    path('crear/', views.usuarioCreate, name='crearU'),
    path('modificar/<str:pk>/', views.usuarioUpdate, name='moodificarU'),
    path('borrar/<str:pk>/', views.usuarioDelete, name='borrarU'),
    path('empleado/listar/', views.empleadoList,name='listarEm'),
    path('empleado/crear/', views.empleadoCreate, name='crearEm'),
    path('empleado/detalle/<str:pk>/', views.empleadoDetail, name='detalleEm'),
    path('empleado/modificar/<str:pk>/', views.empleadoUpdate, name='modificarEm'),
    path('huesped/listar/', views.huespedList,name='listarH'),
    path('huesped/crear/', views.huespedCreate, name='crearH'),
    path('huesped/registro/', views.huespedRegister, name='registro'),
    path('huesped/detalle/<str:pk>/', views.huespedDetail, name='detalleH'),
    path('huesped/descargar/<str:id>/', views.download, name='descargarDoc'),
]