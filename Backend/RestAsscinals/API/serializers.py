from rest_framework import serializers
from .models import Usuario, Empleado, Persona

class UsuarioSerializer(serializers.ModelSerializer): 
    class Meta: 
        model = Usuario 
        fields = '__all__'

class EmpleadoSerializer(serializers.ModelSerializer): 
    class Meta: 
        model = Empleado 
        fields = '__all__'

class PersonaSerializer(serializers.ModelSerializer): 
    class Meta: 
        model = Persona 
        fields = '__all__'