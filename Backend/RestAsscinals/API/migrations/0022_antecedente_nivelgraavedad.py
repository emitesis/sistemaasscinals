# Generated by Django 3.2.4 on 2021-09-22 03:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('API', '0021_documentoantecedente'),
    ]

    operations = [
        migrations.AddField(
            model_name='antecedente',
            name='nivelGraavedad',
            field=models.CharField(default='Medio', max_length=50),
        ),
    ]
