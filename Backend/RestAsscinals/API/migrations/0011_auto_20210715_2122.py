# Generated by Django 3.2.4 on 2021-07-16 01:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('API', '0010_auto_20210709_1119'),
    ]

    operations = [
        migrations.AddField(
            model_name='antecedente',
            name='estado',
            field=models.CharField(default='SOME STRING', max_length=100),
        ),
        migrations.AddField(
            model_name='estadia',
            name='estado',
            field=models.CharField(default='SOME STRING', max_length=100),
        ),
        migrations.AddField(
            model_name='evaluacion',
            name='estado',
            field=models.CharField(default='SOME STRING', max_length=100),
        ),
        migrations.AddField(
            model_name='reserva',
            name='estado',
            field=models.CharField(default='SOME STRING', max_length=100),
        ),
        migrations.AlterField(
            model_name='antecedente',
            name='antecedente',
            field=models.CharField(max_length=1000),
        ),
    ]
