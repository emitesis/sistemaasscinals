# Generated by Django 3.2.4 on 2021-08-22 19:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('API', '0019_evaluacion_observacion'),
    ]

    operations = [
        migrations.AddField(
            model_name='evaluacion',
            name='horaAtencion',
            field=models.CharField(default='2021-08-22 13:34:41', max_length=100),
        ),
        migrations.AddField(
            model_name='reserva',
            name='horaRegistro',
            field=models.CharField(default='2021-08-22 10:52:41', max_length=100),
        ),
    ]
